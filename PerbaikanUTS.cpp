#include <iostream>
using namespace std;

int main() {
  // Membuat variabel x dan y serta memasukkan inputan user
  int x, y;
  cout << "Masukkan bilangan ke 1       : "; 
  cin >> x; 

  // Validasi inputan bilangan pertama
  while (x < 0) {
    cout << "Bilangan ke 1 tidak boleh negatif. Masukkan kembali: ";
    cin >> x;
  }

  cout << "Masukkan bilangan ke 2      :  "; 
  cin >> y;

  // Validasi inputan bilangan kedua
  while (y < 0) {
    cout << "Bilangan ke 2 tidak boleh negatif. Masukkan kembali: ";
    cin >> y;
  }

  // Pengecekan bilangan lebih kecil lebih besar atau pun sama dengan
  if (x < y) {
    cout << "Bilangan Ke 1 < Bilangan Ke 2 :  " << x << " < "<< y << "" << endl;
  } else if (x > y) {
    cout << "Bilangan Ke 1 > Bilangan Ke 2 :  " << x << " > "<< y << "" << endl;
  } else {
     cout << "Bilangan Ke 1 = Bilangan Ke 2 :  " << x << " = "<< y << "" << endl;
  }

  return 0;
}
