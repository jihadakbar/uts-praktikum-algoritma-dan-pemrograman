#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

int main()
{
    system("clear");

    int i = 0, N = 0;
    float Data = 0.0, Rata = 0.0, Total = 0.0;

    cout << "Banyaknya data : ";
    cin >> N;

    cout << "Masukkan data ke-" << i + 1 << " : ";
    cin >> Data;
    Total += Data;
    i++;

    do {
        cout << "Masukkan data ke-" << i + 1 << " : ";
        cin >> Data;
        Total += Data;
        i++;
    } while (i <= 3);

    Rata = Total / N;
    cout << "Banyaknya Data  : " << N << endl;
    cout << "Total Nilai Data : " << fixed << setprecision(2) << Total << endl;
    cout << "Rata rata Nilai data : " << fixed << setprecision(2) << Rata << endl;

    return 0;
}
